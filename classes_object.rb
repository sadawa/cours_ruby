#class
class Book
  attr_accessor  :title ,:author, :pages
  def initialize(title, author,pages)
    @title = title
    @author = author
    @pages = pages
  end
end

book1 = Book.new("Jujutsu kaisen","Gege Akutami",80)
book2 = Book.new("One piece","Oda",60)

# book1.title = "Jujutsu kaisen"
# book1.author = "Gege Akutami"
# book1.pages = 50

# book2.title = "One piece"
# book2.author = "Oda"
# book2.pages = 50


print book1.title
