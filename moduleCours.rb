module Salutations
  def dire_bonjour(nom)
    "Bonjour, #{nom} !"
  end

  def dire_aurevoir(nom)
    "Au revoir, #{nom}."
  end
end

# Pour utiliser ce module, vous pouvez l'inclure dans une classe :
class Personne
  include Salutations
end

# Création d'une instance de Personne et utilisation des méthodes du module
personne = Personne.new
puts personne.dire_bonjour("Alice")
puts personne.dire_aurevoir("Bob")
